package io.craigmiller160.emailservice.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.craigmiller160.apitestprocessor.ApiTestProcessor;
import io.craigmiller160.emailservice.config.EmailConfig;
import io.craigmiller160.emailservice.dto.EmailRequest;
import io.craigmiller160.emailservice.email.EmailService;
import io.craigmiller160.springkeycloakoauth2resourceserver.config.KeycloakResourceServerConfig;
import io.vavr.control.Try;
import java.util.List;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import us.craigmiller160.testcontainers.common.TestcontainersExtension;
import us.craigmiller160.testcontainers.common.core.AuthenticationHelper;

@SpringBootTest
@ExtendWith({TestcontainersExtension.class, SpringExtension.class})
@AutoConfigureMockMvc
public class EmailControllerTest {

  private static final String SUBJECT = "The Subject";
  private static final String MESSAGE = "This is the message";
  private static final String TO_1 = "one@gmail.com";
  private static final String TO_2 = "two@gmail.com";
  private static final String CC_1 = "three@gmail.com";
  private static final String CC_2 = "four@gmail.com";
  private static final String BCC_1 = "five@gmail.com";
  private static final String BCC_2 = "six@gmail.com";

  @MockBean private EmailConfig emailConfig;
  @MockBean private EmailService emailService;

  @Autowired private MockMvc mockMvc;
  @Autowired private ObjectMapper objectMapper;

  @Autowired private KeycloakResourceServerConfig keycloakResourceServerConfig;

  private ApiTestProcessor apiProcessor;
  private static String token;

  @BeforeAll
  public static void tokenSetup() {
    final var authHelper = new AuthenticationHelper();
    final var user = authHelper.createUser("user_%s@gmail.com");
    token = authHelper.login(user).getToken();
  }

  @Test
  public void test_sendEmail() throws Exception {
    final var emailRequest =
        new EmailRequest(
            List.of(TO_1, TO_2), List.of(CC_1, CC_2), List.of(BCC_1, BCC_2), SUBJECT, MESSAGE);

    when(emailService.sendEmail(emailRequest)).thenReturn(Try.success(null));

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/email")
                .header("Authorization", "Bearer %s".formatted(token))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailRequest)))
        .andExpect(MockMvcResultMatchers.status().is(204));

    verify(emailService, times(1)).sendEmail(emailRequest);
  }

  @Test
  @SneakyThrows
  public void test_sendEmail_unauthorized() {
    final var emailRequest =
        new EmailRequest(
            List.of(TO_1, TO_2), List.of(CC_1, CC_2), List.of(BCC_1, BCC_2), SUBJECT, MESSAGE);

    when(emailService.sendEmail(emailRequest)).thenReturn(Try.success(null));

    mockMvc
        .perform(
            MockMvcRequestBuilders.post("/email")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(emailRequest)))
        .andExpect(MockMvcResultMatchers.status().is(401));

    verify(emailService, times(0)).sendEmail(emailRequest);
  }
}
