data "keycloak_realm" "apps_dev" {
  realm = "apps-dev"
}

data "keycloak_realm" "apps_prod" {
  realm = "apps-prod"
}

locals {
  client_common = {
    client_id = "email-service"
    name = "email-service"
    enabled = true
    access_type = "CONFIDENTIAL"
    service_accounts_enabled = true
  }

  access_role_common = {
    name = "access"
  }
}

import {
  id = "apps-dev/98ddff56-205d-45a9-931d-1afea35b109e"
  to = keycloak_openid_client.email_service_dev
}

resource "keycloak_openid_client" "email_service_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}

import {
  to = keycloak_openid_client.email_service_prod
  id = "apps-prod/e1b642e4-8fe6-4ba6-a7fc-05eb19fd9c86"
}

resource "keycloak_openid_client" "email_service_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}

import {
  to = keycloak_role.email_service_access_role_dev
  id = "apps-dev/6da86d0d-7442-442a-93c2-e20601f7262f"
}

resource "keycloak_role" "email_service_access_role_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = keycloak_openid_client.email_service_dev.id
  name = local.access_role_common.name
}

import {
  to = keycloak_role.email_service_access_role_prod
  id = "apps-prod/81db67ff-02e9-4529-8710-c35c841d7b6e"
}

resource "keycloak_role" "email_service_access_role_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = keycloak_openid_client.email_service_prod.id
  name = local.access_role_common.name
}